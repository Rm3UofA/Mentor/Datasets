# Datasets

In this repository it is possible to download some datasets created starting from [here](http://downloads.dbpedia.org/2016-10/core-i18n/en/raw_tables_en.ttl.bz2).

## Tables
A json format dataset of all the wikitables parsed. It contains about 550K tables. They are exported from a MongoDB collection through the mongoexport command

Each line of the file is a json table structured like this:

```json
{
    "_id" : "MongoDB internal id",
    "className" : "Used by Morphia to map with entities",
    "id" : "The id field from the dump by DBpedia",
    "cells" : [ 
        {
            "className" : "Used by Morphia to map with entities",
            "innerHTML" : "HTML code of the cell",
            "isHeader" : "true if this cell is a header",
            "type" : "Old unused field",
            "Coordinates" : "Object containing the coordinates of the cell, starting from 0",
            "cleanedText" : "Surface text of the cell"
        }, 
        ....
    ],
    "beginIndex" : "Field from the dump by DBpedia",
    "endIndex" : "Field from the dump by DBpedia",
    "referenceContext" : "Name of the Wikipedia page the table was taken from",
    "type" : "Field from the dump by DBpedia",
    "classe" : "Field from the dump by DBpedia",
    "maxDimensions" : "Object containing the dimensions of the table, they are indexes so they start both from 0",
    "headersCleaned" : "Array of headers of the table, ordered lexicographically and cleaned from extra HTML fields",
    "keyColumn" : "Number of the key column according to T2K heuristic"
}
```

## BiColumns
A json format dataset of all the bicolumns created. It contains about 824K bicolumns. They are exported from a MongoDB collection through the mongoexport command

Each line of the file is a json table structured like this:

```json
{
    "_id" : "MongoDB internal id",
    "className" : "Used by Morphia to map with entities",
    "rows" : [ 
        {
            "left" : {
                "LinkBlue" : "true if the link is to an existing entity, false if it is red",
                "Title" : "surface text of the link",
                "Href" : "wikiID of the entity linked",
                "Type" : "DBpedia type of the entity linked"
            },
            "right" : {
                "LinkBlue" : "true if the link is to an existing entity, false if it is red",
                "Title" : "surface text of the link",
                "Href" : "wikiID of the entity linked",
                "Type" : "DBpedia type of the entity linked"
            },
            "relations" : [ 
                {
                    "predicate" : "DBpedia relation name between the two entities of the line",
                    "side" : "true if from left to right, false otherwise"
                },
                ...
            ]
        }, 
        ....
    ],
  	"parentTable" : "MongoDB id of the table where this bicolumn is originated",
    "titleLeft" : "header of the left column",
    "titleRight" : "header of the right column",
    "originalColumnLeft" : "column id of the original table of the bicolumn left column",
    "originalColumnRight" : "column id of the original table of the bicolumn right column",
    "typeRight" : "type of the right column if assigned",
    "typeLeft" : "type of the left column if assigned",
    "typeLeftPerc" : "max cov found in the left column",
    "typeRightPerc" : "max cov found in the right column",
    "originalColumnKeyIfPresent" : "column id of the key column of the original table if present in this bicolumn",
    "typeLeftMaybe" : "type found with cov according to typeLeftPerc",
    "typeRightMaybe" : "type found with cov according to typeRightPerc"
}
```